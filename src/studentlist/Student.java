package StudentList;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {
    private String program;
    private String name;
    private String id;
    
    public Student(String n, String i, String prog) {
        name = n;
        id = i;
        program = prog;
    }
     
    public Student ()
    {
    name = null;
    id=null;
    }

    public String getProgram() {
        return program;
    }
    public String getName() {
        return name;
    }
    public String getId() {
        return id;
    }
    
    public void setProgram(String prog) {
        program = prog;
    }
    public void setName(String n) {
        name = n;
    }
    public void setId(String i) {
        id = i;
    }
    
    

}

