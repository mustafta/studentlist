package StudentList;

import java.util.Scanner;
//import studentlist.Student;

/**
 * This class is a simple example of creating arrays of objects
 *
 * @author Paul Bonenfant
 */
public class StudentList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Student[] students = new Student[2];
        
        Scanner input1 = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);
        
        for (int i = 0; i < students.length; i++) {
        
            System.out.println("Enter the student's name");
            String name = input1.nextLine();
            System.out.println("Enter the student's Id");
            String id = input2.nextLine();
            System.out.println("Enter the student's program");
            String program = input2.nextLine();
            Student student = new Student(name, id, program);
                       
            students[i] = student;       
        }
        
        System.out.println("Printing the students:");
        String format = "The student's name is %s , their id is %S, and their program is %S \n";
         
        for (Student student: students) {
        
            System.out.printf(format, student.getName(), student.getId(), student.getProgram());
            
        }

    }

}

